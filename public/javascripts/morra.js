function randomIntBetween(min, max) {
  var rand = Math.random();
  var randBetween = (rand * (max - min)) + min;
  return Math.floor(randBetween);
}

Array.prototype.random = function(){
  var i = randomIntBetween(0, this.length);
  return this[i];
};

///////////////

var Hand = function(params){
  this.name = params.name;
  this.wins = params.wins;
};
Hand.prototype.winsOver = function(otherHand){
  return (otherHand.name == this.wins.name);
};
Hand.prototype.losesOver = function(otherHand){
  return (otherHand.name != this.name) && !this.winsOver(otherHand);
};

Hand.rock     = new Hand({name: 'Rock',     wins: {name: 'Scissors'}});
Hand.paper    = new Hand({name: 'Paper',    wins: Hand.rock});
Hand.scissors = new Hand({name: 'Scissors', wins: Hand.paper});
Hand.rock.wins = Hand.scissors; // close the circle

///////////////

var Player = function(name, choicesEnumerable){
  this.name = name;
  this.choices = choicesEnumerable;
  this.chosen = undefined;
};
Player.prototype.winsOver = function(otherPlayer){
  // Delegate to Hand
  return this.chosen.winsOver(otherPlayer.chosen);
};
Player.prototype.choose = function(){
  throw new Error("Implement on subclass!");
};


var HumanPlayer = function(name, choicesEnumerable){
  Player.apply(this, arguments);
};
HumanPlayer.prototype = Object.create(Player.prototype);
HumanPlayer.prototype.choose = function(choice){
  if (this.choices.indexOf(choice) > -1)
    this.chosen = choice;
  else
    throw new Error("Invalid choice");
};


var RandomPlayer = function(name, choicesEnumerable){
  Player.apply(this, arguments);
};
RandomPlayer.prototype = Object.create(Player.prototype);
RandomPlayer.prototype.choose = function(){
  this.chosen = this.choices.random();
};

////////////////

var SinglePlayerGame = function(players){
  this.human = players.human;
  this.bot = players.bot;
};
SinglePlayerGame.prototype.play = function(newStatus){
  this.human.choose(newStatus.humanChosenHand);
  this.bot.choose();
};
SinglePlayerGame.prototype.decideWinner = function() {
  if (this.human.winsOver(this.bot))
    return this.human;
  else if (this.bot.winsOver(this.human))
    return this.bot;
  else
    return; // pareggio
};
