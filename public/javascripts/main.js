var app = angular.module('app', []);

app.service('Hands', function(){
  return [Hand.rock, Hand.paper, Hand.scissors];
});

app.controller('mainController', function($scope, Hands){
  $scope.hands = Hands;

  $scope.game = new SinglePlayerGame({
    human: new HumanPlayer("You", Hands),
    bot:   new RandomPlayer("Him", Hands)
  });

  $scope.chooseAndPlay = function(hand){
    $scope.game.play({humanChosenHand: hand});
    $scope.winner = $scope.game.decideWinner();
  };

});
