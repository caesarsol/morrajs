describe('Hands', function(){

  beforeEach(function(){
    this.hand = new Hand({name: 'a', wins: undefined});

    this.loserHand = new Hand({name: 'c', wins: this.hand});
    this.winnerHand = new Hand({name: 'b', wins: this.loserHand});
  });

  it('has a name', function() {
    expect(this.hand.name).toBe('a');
  });

  it('wins over a loser handbol', function() {
    expect(this.winnerHand.winsOver(this.loserHand)).toBe(true);
  });

});


describe('Array.random', function(){

  beforeEach(function(){
    this.array = [1,2,3];
    this.distr = {1: 0, 2: 0, 3: 0};
    for (var i = 0; i < 100000; i++)
      this.distr[this.array.random()]++;
  });

  it('returns equally distributed random elements', function() {
    expect(this.distr[2]).toBeCloseTo(100000/3, -3);
  });

});


describe('Player', function(){

  beforeEach(function(){
    this.hand = {winsOver: function(s){return true;} };
    this.player = new Player('testPlayer', []);
    this.player.chosen = this.hand;
    this.player2 = new Player('testPlayer2', []);
    this.player2.chosen = this.hand;
  });

  it('delegates winsOver to the current choice', function() {
    spyOn(this.hand, 'winsOver');
    this.player.winsOver(this.player2);
    expect(this.hand.winsOver).toHaveBeenCalledWith(this.hand);
  });

  it('returns what Hand.winsOver returns', function() {
    var res = this.player.winsOver(this.player2);
    expect(res).toBe(true);
  });

});


describe('HumanPlayer', function(){

  beforeEach(function(){
    this.player = new HumanPlayer('testHumanPlayer', [1,2,3]);
  });

  it('.choose(c) should correctly set .chosen', function() {
    expect(this.player.chosen).toBeUndefined();
    this.player.choose(3);
    expect(this.player.chosen).toBe(3);
  });

  it('.choose(c) should raise if "c" not in choices', function() {
    expect(function(){
      this.player.choose(4);
    }).toThrow();
  });

});


describe('RandomPlayer', function(){

  beforeEach(function(){
    this.choices = [1,2,3];
    spyOn(this.choices, 'random').and.callThrough();
    this.player = new RandomPlayer('testBotPlayer', this.choices);
  });

  it('.choose() should correctly set .chosen', function() {
    expect(this.player.chosen).toBeUndefined();
    this.player.choose();
    expect(this.player.chosen).toBeDefined();
  });

  it('should .choose() randomly', function() {
    this.player.choose();
    expect(this.choices.random).toHaveBeenCalled();
  });

  it('should .choose() only within choices', function() {
    for (var i = 0; i <= 10; i++) {
      this.player.choose();
      expect(this.player.choices).toContain(this.player.chosen);
    }
  });

});


describe('SinglePlayerGame', function(){

  beforeEach(function(){
    this.human = jasmine.createSpyObj('humanPlayer', ['choose', 'winsOver']);
    this.bot   = jasmine.createSpyObj('botPlayer',   ['choose', 'winsOver']);
    this.game = new SinglePlayerGame({
      human: this.human,
      bot:   this.bot
    });
  });

  describe('.play()', function() {
    it('calls .choose() on every player', function() {
      this.game.play({humanChosenHand: 'someHand'});
      expect(this.human.choose).toHaveBeenCalledWith('someHand');
      expect(this.bot.choose).toHaveBeenCalled();
    });
  });

  describe('.decideWinner()', function() {
    it('calls .winsOver() on every player', function() {
      this.game.decideWinner();
      expect(this.human.winsOver).toHaveBeenCalledWith(this.bot);
      expect(this.bot.winsOver).toHaveBeenCalledWith(this.human);
    });

    it('decides the correct winner 1', function() {
      this.human.winsOver = function(){return true;};
      var winner = this.game.decideWinner();
      expect(winner).toBe(this.human);
    });

    it('decides the correct winner 2', function() {
      this.bot.winsOver = function(){return true;};
      var winner = this.game.decideWinner();
      expect(winner).toBe(this.bot);
    });
  });

});
