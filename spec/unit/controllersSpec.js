describe('mainController', function(){

  beforeEach(module('app')); // registers angular-mocks on angular app 'app'

  var $controller;

  beforeEach(inject(function(_$controller_){
    $controller = _$controller_;
  }));

  it('should create "Hands" model with 3 items', function() {
    var scope = {};
    var ctrl = $controller('mainController', {$scope: scope});

    expect(scope.hands.length).toBe(3);
  });

});
