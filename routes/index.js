var express = require('express');
var router = express.Router();
var fs = require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
  fs.readFile('views/index.html', function(err, content){
    res.send(content.toString());
  });
});

module.exports = router;
