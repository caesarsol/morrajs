# MorraJS

An implementation of the "morra cinese" game (Rock Paper Scissors), using AngularJS.

Written trying to follow the SOLID principles.

NodeJS backend serves static content, AngularJS does all the work

## Dev

Run `npm start`, then go to http://localhost:3000/

## Testing

To start an auto-watching continuous testing, run `npm test-dev`.

To start a one-time test, run `npm test.`

## TODO

 * Nicer GUI
 * End to end tests
 * Order in the code: Browserify and `require()`
 * States-based Routing: https://github.com/angular-ui/ui-router

## Author

Cesare Soldini, cesare.soldini@gmail.com
